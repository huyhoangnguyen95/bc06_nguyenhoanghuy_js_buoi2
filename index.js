// bài 1
/**
 *  input : lương ngày , số ngày làm
 *
 *  các bước xử lý : dom tới input và gán giá trị 2 biến salaryOneDay , totalDay
 *
 *  output : tổng tiền lương
 */

function tinhLuong() {
  var salaryOneDay = document.getElementById("txt-luong-ngay").value * 1;
  var totalDay = document.getElementById("txt-so-ngay-lam").value * 1;
  var result = salaryOneDay * totalDay;
  document.getElementById("result1").innerHTML = ` 
  Tiền lương của bạn là : ${result} 
  `;
}

// bài 2
/**
 *  input : nhập vào 5 số
 *
 *  các bước xử lý : dom tới input và gán giá trị 5 biến number1 , number2 , number3 , number4 , number5
 *  (number1 + number2 + number3 + number4 + number5)/5
 *
 *  output : trung bình 5 số
 */

function tinhTrungBinh() {
  var number1 = document.getElementById("txt-num1").value * 1;
  var number2 = document.getElementById("txt-num2").value * 1;
  var number3 = document.getElementById("txt-num3").value * 1;
  var number4 = document.getElementById("txt-num4").value * 1;
  var number5 = document.getElementById("txt-num5").value * 1;
  var result = (number1 + number2 + number3 + number4 + number5) / 5;
  document.getElementById("result2").innerHTML = ` 
  Trung bình 5 số là : ${result}
  `;
}

// bài 3
/**
 *  input : nhập số tiền usd , 1$ = 23500 VND
 *
 *  các bước xử lý : dom tới input và gán giá trị biến amountOfMoney , oneDollar
 *
 *  output : tổng tiền được quy đổi ra VND
 */

function quyDoiTien() {
  var oneDollar = 23500;
  //   oneDollar = new Intl.NumberFormat("vn-VN").format(oneDollar);
  console.log("oneDollar: ", oneDollar);
  var amountOfMoney = document.getElementById("txt-so-tien").value * 1;
  console.log("amountOfMoney: ", amountOfMoney);
  var result = oneDollar * amountOfMoney;
  result = new Intl.NumberFormat("vn-VN").format(result);
  console.log("result: ", result);

  document.getElementById("result3").innerHTML = ` ${result} `;
}

// bài 4

/**
 *  input : chiều dài , chiều rộng
 *
 *  các bước xử lý : dom tới input và gán giá trị cho biến length , width
 *  chu vi = (length + width) * 2
 *  diện tích = length * width
 *
 *  output : chu vi , diện tích
 */

function tinhDienTich() {
  var length = document.getElementById("txt-chieu-dai").value * 1;
  var width = document.getElementById("txt-chieu-rong").value * 1;
  var chuVi = (length + width) * 2;
  var dienTich = length * width;
  document.getElementById("result4").innerHTML = ` 
  Diện tích : ${dienTich} ; 
  Chu vi : ${chuVi}
  `;
}

// bài 5

/**
 *  input : nhập số có 2 chữ số
 *
 *  các bước xử lý : dom tới input và gán giá trị cho biến ten , unit
 *  ten + unit
 *
 *  output : tổng của 2 ký số
 *
 */

function tinhTong() {
  var number = document.getElementById("txt-number").value * 1;
  var ten = Math.floor(number / 10);
  var unit = number % 10;
  var result = ten + unit;
  document.getElementById("result5").innerHTML = ` 
  Tổng 2 ký số là : ${result}
   `;
}
